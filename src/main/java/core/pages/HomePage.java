package core.pages;

import core.common.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;


public class HomePage extends AbstractPage {

    @FindBy(xpath="//div[@id='personal_information']/header[@class='profile-heading']/h1")
    private WebElementFacade pageTitle;

    @FindBy(xpath = "//div[text()=\"Ім'я\"]/following-sibling::div/div[@class='profile-info-l-i-text']")
    private WebElementFacade userName;

    @FindBy(xpath="//div[text()='Електронна пошта']/following-sibling::div/div[@class='profile-info-l-i-text']")
    private WebElementFacade userEmail;

    public HomePage(final WebDriver driver) {
        super(driver);
    }

    public String getPageTitle() {
        return pageTitle.getText();
    }

    public String getUsername(){
        return userName.getText();
    }

    public String getUserEmail(){
        return userEmail.getText();
    }
}