package core.pages;

import core.common.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import net.serenitybdd.core.annotations.findby.FindBy;

@DefaultUrl("https://my.rozetka.com.ua")
public class LoginPage extends AbstractPage {

    public LoginPage(final WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@name='login' and @class='input-text auth-input-text']")
    private WebElementFacade userEmailOrPhoneInput;

    @FindBy(xpath = "//input[@name='password']")
    private WebElementFacade userPasswordInput;

    @FindBy(xpath = "//button[@class='btn-link btn-link-blue btn-link-sign']")
    private WebElementFacade loginBtn;


    public void setUsername(String username){
        typeInto(userEmailOrPhoneInput, username);
    }

    public void setPassword(String password){
        typeInto(userPasswordInput, password);
    }

    public void clickLoginBtn(){
        clickOn(loginBtn);
    }
}
