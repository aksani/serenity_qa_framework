package core.api;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.stream.Collectors;


public class HttpRequests {

    private String userAgent;
    private String cookie;

    public HttpRequests(final WebDriver driver){
        this.cookie = driver.manage().getCookies().stream().map(e -> e.toString().split(";")[0])
                .collect(Collectors.joining(";"));
        this.userAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent");
    }


    public ClientResponse post(final String uri, MediaType mediaType, final String body) throws IOException {

        final Client client = Client.create();
//        client.addFilter(new HTTPBasicAuthFilter("aksanim@ukr.net","123123"));
        return  client
                .resource(uri)
                .header("Cookie", cookie)
                .header("User-Agent", userAgent)
                .type(mediaType)
                .post(ClientResponse.class ,body);

    }


    public void get(final String uri){

        final Client client = new Client();
        client.resource(uri);

    }

}
