package com.hillel.steps.definitions;

import com.hillel.steps.HomePageSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.hillel.steps.LoginPageSteps;

public class HomePageScenarios {

    @Steps
    private HomePageSteps homePageSteps;

    @Steps
    private LoginPageSteps loginPageSteps;

    //Scenario: Open Home Page
    @Given("the user is on the Login Page page")
    public void openLoginPage() {
        loginPageSteps.openLoginPage();
    }

    @When("the user type username '$username' and password '$password'")
    public void typeCredentials(String username, String password){
        loginPageSteps.insertCredentials(username, password);
    }

    @When("the user press $buttonName button")
    public void pressLoginBtn(){
        loginPageSteps.pressLoginBtn();
    }

    @Then("the Home Page with title '$title' should appear")
    public void vefiryPageTitle(String title){
        homePageSteps.verifyPageTitle(title);
    }

}