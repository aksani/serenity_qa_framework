package com.hillel.steps;

import core.pages.LoginPage;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class LoginPageSteps {

    LoginPage loginPage;

    @Step
    public void openLoginPage() {
        loginPage.open();
    }

    @Step
    public void insertCredentials(String username, String password) {
        loginPage.setUsername(username);
        loginPage.setPassword(password);
    }

    @Step
    public void pressLoginBtn() {
        loginPage.clickLoginBtn();
    }

}