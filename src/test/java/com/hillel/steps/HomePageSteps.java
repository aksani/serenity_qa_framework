package com.hillel.steps;

import core.pages.HomePage;
import net.thucydides.core.annotations.Step;

import static org.junit.Assert.assertEquals;


public class HomePageSteps {

    private HomePage homePage;

    @Step
    public void verifyPageTitle(String title){
        assertEquals(homePage.getPageTitle(), title);
    }
}
